package com.example.android.newsapplication;

import android.app.LoaderManager;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class NewsActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<NewsArticle>> {

public static final String LOG_TAG = NewsActivity.class.getName();
public static final String GUARDIAN_REQUEST_URL = "https://content.guardianapis.com/search?section=technology&format=json&show-tags=contributor&api-key=dd7058a0-a299-47e0-931d-bcbaa403b344";
private NewsArticleAdapter mAdapter;
private static final int NEWS_LOADER_ID = 1;
private TextView mEmptyStateView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView newsListView = (ListView) findViewById(R.id.list);

        mAdapter = new NewsArticleAdapter(this, new ArrayList<NewsArticle>());

        mEmptyStateView = (TextView) findViewById(R.id.empty_view);

        newsListView.setEmptyView(mEmptyStateView);

        newsListView.setAdapter(mAdapter);

        newsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                NewsArticle currentNewsArticle = mAdapter.getItem(position);

                Uri newsUri = Uri.parse(currentNewsArticle.getUrl());

                Intent websiteIntent = new Intent (Intent.ACTION_VIEW, newsUri);

                startActivity(websiteIntent);

            }
        });

        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = conMgr.getActiveNetworkInfo();

        if(networkInfo !=null && networkInfo.isConnected()){
            LoaderManager loaderManager = getLoaderManager();
            loaderManager.initLoader(NEWS_LOADER_ID, null, this);
        }else{
            View loadingIndicator = findViewById(R.id.loading_indicator);
            loadingIndicator.setVisibility(View.GONE);
            mEmptyStateView.setText(R.string.no_internet_connection);

        }
    }

    @Override
    public Loader<List<NewsArticle>> onCreateLoader(int i, Bundle bundle) {
        return new NewsLoader(this, GUARDIAN_REQUEST_URL);
    }

    @Override
    public void onLoadFinished(Loader<List<NewsArticle>> loader, List<NewsArticle> newsArticles) {
        View loadingIndicator = findViewById(R.id.loading_indicator);

        loadingIndicator.setVisibility(View.GONE);

        mEmptyStateView.setText(R.string.no_news_found);

        mAdapter.clear();

        if(newsArticles != null && !newsArticles.isEmpty()){
            mAdapter.addAll(newsArticles);
        }


    }

    @Override
    public void onLoaderReset(Loader<List<NewsArticle>> loader) {
        mAdapter.clear();
    }
}
