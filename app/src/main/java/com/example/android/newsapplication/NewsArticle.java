package com.example.android.newsapplication;

import java.util.Date;

/**
 * Created by simon on 2018-06-21.
 */

public class NewsArticle {

    private String mWebTitle;

    private String mSectionName;

    private String mAuthorName;

    private String mPubDate;

    private String mUrl;


    public NewsArticle(String webTitle, String sectionName, String authorName, String pubDate, String url){
        mWebTitle = webTitle;
        mSectionName = sectionName;
        mAuthorName = authorName;
        mPubDate = pubDate;
        mUrl = url;
    }


    public String getWebtitle(){
        return mWebTitle;
    }

    public String getSectionName(){
        return mSectionName;
    }

    public String getAuthorName(){
        return mAuthorName;
    }
    public String getPubdate(){
        return mPubDate;
    }
    public String getUrl(){
        return mUrl;
    }

}
