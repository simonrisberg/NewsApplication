package com.example.android.newsapplication;

import android.content.AsyncTaskLoader;
import android.content.Context;

import java.util.List;

/**
 * Created by simon on 2018-06-23.
 */

public class NewsLoader extends AsyncTaskLoader<List<NewsArticle>> {

    private static final String LOG_TAG = NewsLoader.class.getName();

    private String mUrl;

    public NewsLoader(Context context, String url){
        super(context);
        mUrl = url;
    }
    @Override
    protected void onStartLoading(){
        forceLoad();
    }
    @Override
    public List<NewsArticle> loadInBackground() {
        if(mUrl == null){
            return null;
        }
        List<NewsArticle> news = QueryUtils.fetchNewsData(mUrl);

        return news;
    }
}
